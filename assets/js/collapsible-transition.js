/*

[data-collapsible] {
	--outer-height: auto;
 height: var(--outer-height);
}

*/

const autoHeight = function () {
	"use strict";

	const collapsibles = document.querySelectorAll ("[data-collapsible]");
	var l = collapsibles.length;

	console.log(collapsibles);

	while (l--) {
		let c = collapsibles[l];
		let outerHeight = c.offsetHeight;
		c.style.setProperty ('--outer-height', outerHeight);
		console.log(outerHeight);
	}

};
